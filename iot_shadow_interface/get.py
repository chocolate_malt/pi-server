import boto3
import json
from botocore import exceptions as boto_exc
import logging


logger = logging.getLogger(__name__)
client = boto3.client('iot-data')


class ThingNotFoundError(Exception):
    pass


def get_thing_shadow(iot_id: str) -> dict:
    try:
        shadow = client.get_thing_shadow(thingName=iot_id)
    except boto_exc.ClientError:
        logger.error("Error")
        raise ThingNotFoundError("Unable to find iot device with name={}.".format(iot_id))
    shadow_dict = json.loads(shadow['payload'].read().decode('ascii'))
    return shadow_dict


def update_thing_shadow(iot_id: str, payload: dict) -> dict:
    # best to take payload in as a dict here, and then convert it to bytes
    payload = bytearray(json.dumps(payload), encoding='ascii')
    return client.update_thing_shadow(thingName=iot_id, payload=payload)  # probably need to decode this
