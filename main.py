from api.routes import api
import json
import logging
from database.orm import crud
import datetime as dt

logger = logging.getLogger(__name__)

VALID_DEVICE_TYPES = ['led', 'relay', 'temperaturesensor']


def process_sns_message(event, context):
    logger.info("Processing SNS Message:")

    message = event['Records'][0]['Sns']['Message']
    logger.info(message)
    message = json.loads(message)
    message_timestamp = message['timestamp']
    board = crud.get_board(message['clientToken'])
    logger.info("Board Found: {}".format(board))
    reported_state = message['current']['state']['reported']
    reported_metadata = message['current']['metadata']['reported']
    for device_type_name, device_type_states in reported_state.items():
        if device_type_name in VALID_DEVICE_TYPES:
            for device_name, device_state in device_type_states.items():
                timestamp = reported_metadata[device_type_name][device_name]['timestamp']
                if timestamp == message_timestamp:  # otherwise it's not from latest report
                    device = crud.get_device(board, device_type_name, device_name)
                    crud.record_new_device_state(device, str(device_state),dt.datetime.fromtimestamp(message_timestamp))


# We only need this for local development.
if __name__ == '__main__':

    logging.basicConfig()
    logging.root.setLevel(logging.DEBUG)

    # populate with initial data
    example_user = crud.find_or_create_user('coxy.t82@gmail.com', 'foobar', first_name='Tony', last_name='Cox')
    example_board = crud.find_or_create_board(example_user, 'brew_controller_pi')
    example_device_dict = {
        "led": ["led_1", "led_2"],
        "relay": ["relay_1"],
        "pwm": [],
        "temperaturesensor": ["temp_1", "temp_2"],
        "floatSensor": [],
        "solenoidValve": [],
    }
    example_devices = []
    for dev_type, devs in example_device_dict.items():
        for dev_name in devs:
            example_devices.append(crud.find_or_create_device(example_board, dev_type, dev_name))
    with open("/home/tc/Downloads/sns-dump-event (5)") as f:
        foo = json.load(f)
        process_sns_message(foo, None)

    if False:
        api.run()
