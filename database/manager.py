from flywheel import Engine
import logging
from typing import Any


logger = logging.getLogger(__name__)


class EngineWrapper(object):
    # singleton wrapper of an engine object
    class __EngineWrapper(object):
        def __init__(self):
            self.engine = Engine()
            self.engine.connect_to_region('ap-southeast-2')

    instance = None

    def __getattribute__(self, name: str) -> Any:
        return getattr(self.instance, name)

    def __new__(cls) -> Any:
        if not EngineWrapper.instance:
            EngineWrapper.instance = EngineWrapper.__EngineWrapper()
        return EngineWrapper.instance

    def __setattr__(self, name: str, value: Any) -> None:
        return setattr(self.instance, name, value)


engine = EngineWrapper().engine  # type: Engine

