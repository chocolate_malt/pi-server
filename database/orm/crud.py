from . import models
from typing import Collection
import datetime as dt
import logging




logger = logging.getLogger(__name__)


def find_or_create_user(email_address: str, password: str, first_name: str=None, last_name: str=None,
                        middle_names: Collection[str]=None) -> models.User:
    email_user = models.EmailUser.find(email_address)
    if email_user:
        user = email_user.user
        logger.debug("Found existing user {}".format(user))
    else:
        kwargs = {}
        if first_name:
            kwargs.update(dict(first_name=first_name))
        if last_name:
            kwargs.update(dict(last_name=last_name))
        if middle_names:
            kwargs.update(dict(middle_names=middle_names))
        user = models.User(email_address, password, **kwargs)
        email_user = models.EmailUser(email_address=email_address, user_id=user.user_id)
        user.save()
        email_user.save()
    return user


def find_or_create_board(owner: models.User, name: str):
    name_board = models.NameBoard.find(name)
    if name_board:
        board = name_board.board
        logger.debug("Found existing board {}".format(board))
    else:
        board = models.Board(owner.user_id, name)
        name_board = models.NameBoard(name=name, board_id=board.board_id)
        board.save()
        name_board.save()
    return board


def find_or_create_device(board: models.Board, device_type: str, device_name: str):
    device = models.Device.find(board.board_id, device_type, device_name)
    if device:
        logger.debug("Found existing device {}".format(device))
    else:
        device = models.Device(board_id=board.board_id, device_type=device_type, name=device_name)
        device.save()
    return device


def get_board(name: str) -> models.Board:
    name_board = models.NameBoard.from_name(name)
    return name_board.board


def get_device(board: models.Board, device_type: str, device_name:str) -> models.Device:
    device = models.Device.from_name(board.board_id, device_type, device_name)
    return device


def record_new_device_state(device: models.Device, device_state: str, date_time: dt.datetime) -> models.DeviceState:
    device_state = models.DeviceState(
        device_id=device.device_id,
        date_time=date_time,
        board_id=device.board_id,
        reported_state=device_state,
    )
    device_state.save()
    return device_state