from flywheel import TypeDefinition, STRING
import uuid
from flywheel.fields.types import register_type


def registered_type(allow_in_set):  # returns a class decorator function
    def register(cls):
        return register_type(cls, allow_in_set=allow_in_set)
    return register


@registered_type(allow_in_set=True)
class UUIDType(TypeDefinition):
    data_type = uuid.UUID
    aliases = ['uuid']
    ddb_data_type = STRING

    def coerce(self, value, force):
        # just try to call the UUID constructor - if it fails, raise TypeError or ValueError
        try:
            if isinstance(value, uuid.UUID):
                return value
            return uuid.UUID(value)
        except TypeError as e:
            raise TypeError("Unable to coerce {} to UUID.  Original Exception: \n {}".format(
                value, str(e)
            ))
        except Exception as e:
            raise ValueError("Unable to coerce {} to UUID.  Original Exception: \n {}".format(
                value, str(e)
            ))

    def ddb_dump(self, value):
        return str(value)

    def ddb_load(self, value):
        return uuid.UUID(value)

