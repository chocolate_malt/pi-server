from flywheel import Model, Field, set_, Composite
import datetime as dt
import bcrypt
import logging
from typing import List, Union, Optional
from database.manager import engine
import uuid
from dynamo3.exception import DynamoDBError
from .exc import ItemNotFoundError


logger = logging.getLogger(__name__)


class ModelWithGlobalEngine(Model):
    __metadata__ = {
        '_abstract': True
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if self.__engine__ is None:
            self.__engine__ = engine


class User(ModelWithGlobalEngine):
    # required fields
    user_id = Field(type=uuid.UUID, hash_key=True)
    email_address = Field(type=str)
    hashed_password = Field(type=bytes)
    created_timestamp = Field(type=dt.datetime)
    # optional fields
    first_name = Field(type=str)
    last_name = Field(type=str)
    middle_names = Field(type=set_(str))

    def __init__(self, email_address: str, password: str, **kwargs):
        user_id = kwargs.pop('user_id', uuid.uuid4())
        created_timestamp = kwargs.pop('created_timestamp', dt.datetime.utcnow())
        super().__init__(user_id=user_id, email_address=email_address, hashed_password=self.hash_password(password),
                         created_timestamp=created_timestamp, **kwargs)

    def delete(self, *args, **kwargs):
        # cascade deletion to other objects
        self.email_user.delete(*args, **kwargs)
        for board in self.boards:
            board.delete(*args, **kwargs)
        return super().delete(*args, **kwargs)

    @classmethod
    def hash_password(cls, password: Union[str, bytes]) -> bytes:
        byte_password = password if type(password) == bytes else password.encode()
        return bcrypt.hashpw(byte_password, bcrypt.gensalt())

    @classmethod
    def from_id(cls, id_: Union[str, uuid.UUID]) -> Optional["User"]:
        # noinspection PyTypeChecker
        user = engine.get(cls, user_id=str(id_))  # type: User
        if not user:
            raise ItemNotFoundError("Unable to find item with id: {}".format(id))
        return user

    def check_password(self, password: Union[str, bytes]) -> bool:
        byte_password = password if type(password) == bytes else password.encode()
        return bcrypt.checkpw(byte_password, self.hashed_password)

    @property
    def email_user(self) -> "EmailUser":
        email_user = engine.get(EmailUser, email_address=self.ddb_dump_field_('email_address'))  # type: EmailUser
        return email_user

    @property
    def boards(self) -> List["Board"]:
        return engine.query(Board).filter(owner_id=self.user_id).all()

    def __repr__(self) -> str:
        return str(self.email_address)


# @engine.register
class EmailUser(ModelWithGlobalEngine):
    # provided as a lookup from email address to user and to enforce uniqueness on email addresses
    email_address = Field(type=str, hash_key=True)
    user_id = Field(type=uuid.UUID)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def from_email_address(cls, email_address: str) -> "EmailUser":
        # noinspection PyTypeChecker
        email_user = engine.get(cls, email_address=email_address)  # type: EmailUser
        if not email_user:
            raise ItemNotFoundError("Unable to find user with email address {}".format(email_address))
        return email_user

    @classmethod
    def find(cls, email_address: str) -> Optional["EmailUser"]:
        try:
            return cls.from_email_address(email_address)
        except (DynamoDBError, ItemNotFoundError):
            return None

    @property
    def user(self) -> User:
        user = engine.get(User, user_id=self.ddb_dump_field_('user_id'))  # type: User
        if not user:
            raise ItemNotFoundError("Unable to find user from email_user {}".format(self))
        return user

    def __repr__(self) -> str:
        return "[email_address: {}; user_id: {}]".format(self.email_address, self.user_id)


# @engine.register
class Board(ModelWithGlobalEngine):
    board_id = Field(type=uuid.UUID, hash_key=True)
    owner_id = Field(type=uuid.UUID)
    name = Field(type=str)

    def __init__(self, owner_id, name, **kwargs):
        board_id = kwargs.pop('board_id', uuid.uuid4())
        super().__init__(board_id=board_id, owner_id=owner_id, name=name, **kwargs)

    def delete(self, *args, **kwargs):
        # cascade to dependent objects
        for device in self.devices:
            device.delete(*args, **kwargs)
        return super().delete(*args, **kwargs)

    @property
    def owner(self) -> User:
        user = engine.get(User, user_id=self.ddb_dump_field_('owner_id'))  # type: User
        if not user:
            raise ItemNotFoundError("Unabel to find owner for board {}".format(self))
        return user

    @property
    def devices(self) -> List["Device"]:
        return engine.query(Device).filter(Device.board_id == self.board_id).all()

    def __repr__(self) -> str:
        return "[owner: {}; id: {}; name: {}]".format(self.owner_id, self.board_id, self.name)


class NameBoard(ModelWithGlobalEngine):
    name = Field(type=str, hash_key=True)
    board_id = Field(type=uuid.UUID)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @classmethod
    def from_name(cls, name: str) -> Optional["NameBoard"]:
        # noinspection PyTypeChecker
        name_board = engine.get(cls, name=name)  # type: NameBoard
        return name_board

    @classmethod
    def find(cls, name: str) -> Optional["NameBoard"]:
        try:
            return cls.from_name(name)
        except (DynamoDBError, ItemNotFoundError):
            return None

    @property
    def board(self) -> Board:
        board = engine.get(Board, board_id=self.ddb_dump_field_("board_id"))  # type: Board
        if not board:
            raise ItemNotFoundError("Could not find board with name {}".format(self.name))
        return board

    def __repr__(self) -> str:
        return "[name: {}; board_id: {}]".format(self.email_address, self.board_id)


# @engine.register
class Device(ModelWithGlobalEngine):
    board_id = Field(type=uuid.UUID, hash_key=True)
    device_type = Field(type=str, index='type-idx')
    name = Field(type=str, index='name-idx')
    device_id = Composite('device_type', 'name', data_type=str, range_key=True)

    def __init__(self, board_id: str, device_type: str, name: str, **kwargs):
        super().__init__(board_id=board_id, device_type=device_type, name=name, **kwargs)

    def delete(self, *args, **kwargs):
        # cascade to dependent objects
        for state in self.states:
            state.delete(*args, **kwargs)
        return super().delete(*args, **kwargs)

    @property
    def board(self) -> Board:
        board = engine.get(Board, board_id=self.ddb_dump_field_('board_id'))  # type: Board
        if not board:
            raise ItemNotFoundError("Unable to find board for device {}".format(self))
        return board

    @property
    def owner(self) -> User:
        owner = self.board.owner
        if not owner:
            raise ItemNotFoundError("Unable to find owner for device {}".format(self))
        return owner

    @property
    def states(self) -> List["DeviceState"]:
        return engine.query(DeviceState).filter(
            DeviceState.device_id == self.device_id,
            DeviceState.board_id == self.board_id
        ).all()

    @classmethod
    def find(cls, board_id: uuid.UUID, device_type: str, name: str) -> Optional['Device']:
        try:
            return cls.from_name(board_id, device_type, name)
        except (DynamoDBError, ItemNotFoundError):
            return None



    @classmethod
    def from_name(cls, board_id: uuid.UUID, device_type: str, name: str) -> 'Device':
        # noinspection PyTypeChecker
        device = engine.get(cls, board_id=str(board_id), device_type=device_type, name=name)  # type: Device
        if not device:
            raise ItemNotFoundError("Unable to find device on board {} with key {}:{}".format(
                board_id, device_type, name
            ))
        return device

    def __repr__(self) -> str:
        return "[board_id: {}; device_id: {}".format(self.board_id, self.device_id)


# @engine.register
class DeviceState(ModelWithGlobalEngine):
    device_id = Field(type=str, hash_key=True)
    date_time = Field(type=dt.datetime, range_key=True)
    board_id = Field(type=uuid.UUID, index='board_idx')
    reported_state = Field(type=str)
    desired_state = Field(type=str)

    @property
    def device(self) -> Device:
        device = engine.get(
            Device,
            board_id=self.ddb_dump_field_('board_id'),
            device_id=self.ddb_dump_field_('device_id')
        )  # type: Device

        return device

    @property
    def board(self) -> Board:
        board = engine.get(Board, board_id=self.ddb_dump_field_('board_id'))  # type: Board
        if not board:
            raise ItemNotFoundError("Unable to find board for deivce state {}".format(self))
        return board

    def __repr__(self) -> str:
        return "[device_id: {}; date_time: {}]".format(self.device_id, self.date_time)


def create_and_update_schema_if_required():
    # Create the dynamo table for our registered model - this will ignore any tables that already exist
    # engine.delete_schema()
    engine.create_schema()
    engine.update_schema()


for cls in (User, EmailUser, Board, NameBoard, Device, DeviceState):
    engine.register(cls)

create_and_update_schema_if_required()
