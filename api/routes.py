from flask import Flask, json, request
import pi_service
import logging
from flask_cors import CORS


api = Flask(__name__)
CORS(api)

logger = logging.getLogger(__name__)

INCOMPLETE_STUB = 'This requested URL has not yet been implemented on the server.'
INVALID_REQUEST = 'Invalid Request'


GET = 'GET'
PUT = 'PUT'
POST = 'POST'


class Result(object):
    SUCCESS = 'success'
    MESSAGE = 'message'
    STATE = 'state'
    ERROR = 'error'
    def __init__(self, success=True, message=None, state=None, error=None) -> None:
        self.success = success  # type: bool
        self.message = message  # type: str
        self.state = state  # type: dict
        self._error = error  # type: Exception
        super().__init__()

    @property
    def error(self):
        return self._error

    @error.setter
    def error(self, value: Exception):
        self.success = False
        self._error = value

    @property
    def error_string(self):
        return str(self.error) if self.error is not None else None

    def as_dict(self):
        return {
            self.SUCCESS: self.success,
            self.MESSAGE: self.message,
            self.STATE: self.state,
            self.ERROR: self.error_string
        }

    def jsonified(self):
        return json.jsonify(self.as_dict())


@api.route('/<string:iot_id>')
def devices(iot_id: str):
    result = Result()
    try:
        result.state = pi_service.PiService(iot_id).get_device_states()
    except Exception as e:
        result.error = e
    return result.jsonified()


@api.route('/<string:iot_id>/<string:device_type>')
def devices_of_type(iot_id: str, device_type: str) -> str:
    result = Result()
    try:
        result.state = pi_service.PiService(iot_id).get_device_states_of_type(device_type)
    except KeyError as e:
        result.error = e
    return result.jsonified()


@api.route('/<string:iot_id>/<string:device_type>/<string:device_name>', methods=[GET, PUT])
def device_state(iot_id: str, device_type: str, device_name: str) -> str:
    service = pi_service.PiService(iot_id)
    result = Result()
    try:
        if request.method == PUT:
            data = json.loads(request.data)
            service.set_desired_state(device_type, device_name, data['state'])
        else:
            result.state = service.get_device_state(device_type, device_name)
    except Exception as e:
        result.error = e
    return result.jsonified()