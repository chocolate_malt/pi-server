# manages the connection to AWS IOT to get/set the pi state
import iot_shadow_interface as iot


STATE = 'state'
REPORTED = 'reported'
DESIRED = 'desired'


class PiService(object):
    def __init__(self, iot_id: str) -> None:
        self.iot_id = iot_id
        self.shadow = iot.get_thing_shadow(iot_id)
        super(PiService, self).__init__()

    def get_device_states(self) -> dict:
        return self.shadow[STATE]

    def get_device_states_of_type(self, type_: str) -> dict:
        devices = {k: v.get(type_) for k, v in self.get_device_states().items()}
        if not devices:
            raise KeyError("Could not find any devices of type_ {}".format(type_))
        return devices

    def get_device_state(self, type_: str, name: str) -> dict:
        devices_of_type = self.get_device_states_of_type(type_)
        device = {k: v.get(name) for k, v in devices_of_type.items() if v is not None}
        if not device:
            raise KeyError("Could not find {} with name {}".format(type_, name))
        return device

    def set_desired_state(self, type_: str, name: str, state: str) -> dict:
        payload = {
            STATE: {
                DESIRED: {
                    type_: {name: state}
                }
            }
        }
        return iot.update_thing_shadow(self.iot_id, payload)
